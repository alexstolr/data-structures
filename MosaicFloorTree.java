//Alex Stoliar 310668629
//Keren Bar    ********
//submission date 15/9/13
/**
 * 
 * @author Alex Stoliar & Keren Bar
 *
 */
class MosaicFloorTree
{
	Node root;
	String Infix;
	Rectangle[] basicRectangles;
	/**
	 * 
	 * @param basicRectangles
	 * @param infix
	 */
	public MosaicFloorTree(Rectangle[] basicRectangles, String infix){ 
		this.Infix=infix;
		this.basicRectangles=basicRectangles;    
		root = RecTree(infix);  
	}


	/**
	 * 
	 * @return infix of mosaic tree.
	 */
	public String getInfix(){
		String str = "";
		if(root.left == null && root.right == null)
			str = root.data.getName();
		else
			str = getInfix(root, "");
		return str;
	}

	/**
	 * 
	 * 
	 */
	public void updateSizes() { 
	}

	/**
	 * 
	 * @return size of black area.
	 */
	public int blackArea(){ 
		return blackArea(root);
	}

	/**
	 * updates X & Y.
	 */
	public void updateXY(){
			root.data.setX(0); 
			root.data.setY(0); 
		updateXY(root);
	}

	/**
	 * 
	 * @param x
	 * @param y
	 * @return name of rectangle in which (x,y) are situated.
	 */
	public String whereAmI(int x, int y){ 
		updateXY();
		if(blackArea()!= 0 || x > root.data.getWidth() || x < 0 || y > root.data.getHight() || y < 0)
			return "-1";
		else
			return whereAmI(root, x, y);
	}

	public String bonusWhereAmI(int x, int y){
		/*if(x<0 || y<0 || x>root.data.getWidth() || y>root.data.getHight())
			return "-1";
		else if(whereAmI(root, x, y).equals(""))
			return "Black Area";
		return whereAmI(root, x, y);*/
		return "";
	}

	/**
	 * 
	 * @param infix.
	 * @return Node.
	 */
	private Node RecTree(String infix){
		if(infix.equals(""))
			return null;
		Node node = new Node(null, null);   
		if(infix.charAt(0) != '(' && infix.charAt(infix.length()-1) != ')'){    //basic case -  when infix is a number.
			node.data = basicRectangles[Integer.parseInt(infix)];
			node.left = null;
			node.right = null;
			node.type = "B";
			return node;
		}
		else{
			if(infix.charAt(2) != '('){  // if infix is of type "( 1 H 2 )" - left node is basic.
				int j = 2;    // j-1 is index of last digit of the leftest int. int the exampleabove index of '1' is 2.
				while(!String.valueOf(infix.charAt(j)).equals(" ") && j < infix.length() - 1 ){ //counts number of digits of the number.
					j++;
				}
				if(infix.length() != j + 2 ){ //  current infix is NOT of type "( 1 H" or "V 2 )"/  .
					node.data = new Rectangle(infix.substring(j+1, j+2), 1, 1);
					node.type = infix.substring(j+1, j+2);
					node.left = RecTree(infix.substring(0,j+2));
					if(infix.charAt(j + 3) == '(') //  curent infix is of type "( 1 H ( 2 V 3 ) ) - right node is not basic.
						node.right = RecTree(infix.substring(j + 3, infix.length() - 2));
					else                           //  curent infix is of type "( 1 H 2 ) right node is basic .
						node.right = RecTree(infix.substring(j + 1));
					node.left.parent = node;
					node.right.parent = node;
					if(infix.substring(j+1, j+2).equals("H")){
						node.data.setWidth(Math.max(node.left.data.getWidth(),node.right.data.getWidth()));
						node.data.setHight(node.left.data.getHight()+ node.right.data.getHight());
					}
					else{
						node.data.setWidth(node.left.data.getWidth()+node.right.data.getWidth());
						node.data.setHight(Math.max(node.left.data.getHight(), node.right.data.getHight()));
					}
				}
				else{    //  curent infix is of type "( 1 H" or "V 2 )"/  .
					node.data = basicRectangles[Integer.parseInt(infix.substring(2, j))];  //creates leaf "B" type.
					node.type ="B"; 
					node.left = null;
					node.right = null;
					return node;
				}
			}
			else {     // if infix is of type "( ( 1 V 2 ) H 3 )" - left node is not basic
				int currDataIndex = 1;
				int counter = 0;
				boolean enter = false;
				while((!enter ||  counter!=0 )&& currDataIndex <= infix.length() -3){  
					enter = true;
					currDataIndex++;
					if(infix.charAt(currDataIndex)=='('){
						counter++;
					}
					if(infix.charAt(currDataIndex) ==')')
						counter--;
				}
				node.data = new Rectangle(infix.substring(currDataIndex + 2, currDataIndex + 3), 1, 1);
				node.type = infix.substring(currDataIndex + 2, currDataIndex + 3);
				node.left =  RecTree(infix.substring(2, currDataIndex + 1)) ;
				if(infix.charAt(currDataIndex + 4) == '(')
					node.right = RecTree(infix.substring(currDataIndex + 4, infix.length() - 2));
				else
					node.right = RecTree(infix.substring(currDataIndex + 2, infix.length()));
				if(infix.substring(currDataIndex + 2, currDataIndex + 3).equals("H")){

					node.data.setWidth(Math.max(node.left.data.getWidth(), node.right.data.getWidth()));
					node.data.setHight(node.left.data.getHight() +node.right.data.getHight());

				}
				else{ //  V

					node.data.setWidth(node.left.data.getWidth() + node.right.data.getWidth());
					node.data.setHight(Math.max(node.left.data.getHight(),node.right.data.getHight()));
				}
				node.left.parent = node;
				node.right.parent = node;
			}
		}
		return node;
	}


	/**
	 * 
	 * @param root
	 */
	public String getInfix(Node root, String string){

		String str = "";
		if(root.left != null)
			str = str + "( " + getInfix(root.left, str);
		str = str + root.data.getName() + " ";
		if(root.right != null)
			str = str + getInfix(root.right, str) + ") ";
		return str;
	}

	/**
	 * 
	 * @param root
	 * @return
	 */
	public int blackArea(Node root) {

		int blackArea = 0;
		if(root.left == null && root.right == null)
			return blackArea;
		else if(root.type.equals("H")){   // type of this triangle is H
			if(root.left.data.getWidth() > root.right.data.getWidth())   //lower triangle width is bigger then the upper one's.
				blackArea = blackArea(root.left) + blackArea(root.right) + (root.left.data.getWidth() - root.right.data.getWidth()) * root.right.data.getHight();
			else if(root.left.data.getWidth() < root.right.data.getWidth())                                                         //upper triangle width is bigger then the lower one's.
				blackArea = blackArea(root.left) + blackArea(root.right) + (root.right.data.getWidth() - root.left.data.getWidth()) * root.left.data.getHight();
			else
				blackArea = blackArea(root.left) + blackArea(root.right);
		}
		else{                        // type of this triangle is V
			if(root.left.data.getHight() > root.right.data.getHight())
				blackArea = blackArea(root.left) + blackArea(root.right) + (root.left.data.getHight() - root.right.data.getHight()) * root.right.data.getWidth();
			else if(root.left.data.getHight() < root.right.data.getHight())
				blackArea = blackArea(root.left) + blackArea(root.right) + (root.right.data.getHight() - root.left.data.getHight()) * root.left.data.getWidth();
			else
				blackArea = blackArea(root.left) + blackArea(root.right);
		}
		return blackArea;
	}
	
	/**
	 * 
	 * @param root
	 */
	public void updateXY(Node root){
		if(root.data.getX()<0)
			root.data.setX(0);  
		if(root.data.getY()<0)
			root.data.setY(0);  
		if(root.left!=null){
			root.left.data.setX(root.data.getX());
			root.data.setY(root.data.getY());
		}
		if(root.type.equals("H")){
			root.right.data.setX(root.data.getX());
			root.right.data.setY(root.data.getY()+ root.left.data.getHight());
		}
		if(root.type.equals("V")){
			root.right.data.setX(root.data.getX() + root.left.data.getWidth());
			root.right.data.setY(root.data.getY());

		}
		if(root.left!=null)
			updateXY(root.left);
		if(root.right!=null)
			updateXY(root.right);
	}
	/**
	 * 
	 * @param root
	 * @param x
	 * @param y
	 * @return name of rectangle in which (x,y) are situated.
	 */
	public String whereAmI(Node root, int x, int y){
		if(root.type.equals("B"))
			return root.data.getName();
		else{
			if(root.type.equals("H")){
				if(y > root.data.getY()+root.left.data.getHight())
					return whereAmI(root.right, x, y);
				else
					return whereAmI(root.left, x, y);
			}
			else{
				if(x > root.data.getX() + root.left.data.getWidth())
					return whereAmI(root.right, x, y);
				else
					return whereAmI(root.left, x, y);
			}
			//else return "";
		}
	}

}

